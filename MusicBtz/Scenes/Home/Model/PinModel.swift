//
//  PinModel.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 30/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

struct PinModel {

    let location: CLLocationCoordinate2D
    let name: String
    let address: String?
    let id: String
    let lifeSpan: TimeInterval

    init?(_ place: Place) {
        guard let longitude = Double(place.coordinates?.longitude ?? ""),
            let latitude = Double(place.coordinates?.latitude ?? "") else {

                return nil
        }

        location = CLLocationCoordinate2D(
            latitude: latitude,
            longitude: longitude)
        name = place.name ?? "N/A"
        address = place.address
        id = place.id

        lifeSpan = Double(place.lifeSpan!.begin!.prefix(4))! - Constants.openSince
    }
}

class PinItem: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    let id: String
    let lifeSpan: TimeInterval

    init(_ model: PinModel) {
        self.coordinate = model.location
        self.title = model.name
        self.subtitle = model.address
        self.id = model.id
        self.lifeSpan = model.lifeSpan
    }
}
