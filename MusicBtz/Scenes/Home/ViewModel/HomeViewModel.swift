//
//  HomeViewModel.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 30/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol HomeVMVD: class {
    func show(items: [PinItem])
    func remove(item: PinItem)
    func updateLoadingStatus(isLoading: Bool)
    func show(error: String)
}

protocol HomeViewModel {
    var view: HomeVMVD? { get set }

    func search(for query: String?)
}

final class HomeViewModelImpl: HomeViewModel {
    typealias Dependencies = HasNetworkService
    private let dependencies: Dependencies

    private var currentOffset = 0
    private var totalAmount = -1
    private var isLoading = false
    private var pinItems = [PinItem]()

    weak var view: HomeVMVD?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func search(for query: String?) {
        guard let q = query, !q.isEmpty, !isLoading, !shouldStopLoading else {
            return
        }

        resetData()

        isLoading = true
        view?.updateLoadingStatus(isLoading: isLoading)

        DispatchQueue.global(qos: .userInitiated).async { [dependencies, currentOffset] in
            dependencies.networkService.getPlaces(by: q, offset: currentOffset) { [weak self] response in
                guard let self = self else { return }
                
                switch response {
                case .error(let error):
                    self.view?.show(error: error)
                    self.presentData()
                case .success(let data):
                    self.totalAmount = data.count
                    self.currentOffset += 1
                    self.prepareViewModel(with: data.data)

                    if !self.shouldStopLoading {
                        self.donwloadAdditionalData(for: q)
                    } else {
                        self.presentData()
                    }
                }
            }
        }
    }
}

// MARK: - private
private extension HomeViewModelImpl {

    var shouldStopLoading: Bool {
        return Constants.pageSize * (currentOffset) > totalAmount && totalAmount != -1
    }

    func prepareViewModel(with places: [Place]) {
        let newPlaces = places
            .filter {
                guard let beginDate = $0.lifeSpan?.begin else { return false}
                let year = beginDate.prefix(4)

                return Double(year) ?? 0 > Constants.openSince
            }

        let items = newPlaces
            .map { PinModel($0) }
            .compactMap { $0 }
            .map { PinItem($0) }

        pinItems += items
    }

    func donwloadAdditionalData(for query: String) {
        var requestCount = totalAmount / Constants.pageSize - currentOffset
        if totalAmount % Constants.pageSize != 0 {
            requestCount += 1
        }

        let queue = DispatchQueue(label: "home.musicbtz", attributes: .concurrent)
        let dispatchGroup = DispatchGroup()

        var tasks: [DispatchWorkItem] = []
        var errors = Set<String>()

        for index in 1...requestCount {
            dispatchGroup.enter()

            let getPlacesTask = DispatchWorkItem(flags: .inheritQoS) { [dependencies] in
                dependencies.networkService.getPlaces(by: query, offset: index) { [weak self] response in
                    guard let self = self else { return }

                    switch response {
                    case .error(let error):
                        errors.insert(error)
                    case .success(let data):
                        self.currentOffset += 1
                        self.prepareViewModel(with: data.data)
                    }
                    dispatchGroup.leave()
                }
            }
            tasks.append(getPlacesTask)
        }

        for task in tasks {
            queue.async(execute: task)
        }

        dispatchGroup.notify(queue: queue) { [weak self] in
            if !errors.isEmpty {
                self?.view?.show(error: Array(errors).joined(separator: "\n"))
            }
            self?.presentData()
        }
    }

    func presentData() {
        addTimer(for: pinItems)
        isLoading = false
        view?.updateLoadingStatus(isLoading: isLoading)
        view?.show(items: pinItems)
    }

    func resetData() {
        currentOffset = 0
        totalAmount = -1
        pinItems.removeAll()
    }

    func addTimer(for items: [PinItem]) {
        DispatchQueue.main.async { [weak self] in
            items.forEach { [weak self] (item) in
                guard let self = self else { return }

                let timer = Timer.scheduledTimer(
                    timeInterval: item.lifeSpan,
                    target: self,
                    selector: #selector(self.removeItem(timer:)),
                    userInfo: item,
                    repeats: false)
                timer.tolerance = 1
            }
        }
    }

    @objc func removeItem(timer: Timer) {
        guard let item = timer.userInfo as? PinItem else {
            if timer.isValid {
                timer.invalidate()
            }
            return
        }

        pinItems.removeAll { $0.id == item.id }
        view?.remove(item: item)

        if timer.isValid {
            timer.invalidate()
        }
    }
}
