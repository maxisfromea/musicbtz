//
//  Dispatcher.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol Dispatcher {
    func execute(request: Request, completion: @escaping ((DispatcherResponse) -> Void))
}

enum DispatcherResponse {
    case success(Data)
    case error(Error?)
}

final class NetworkDispatcher: Dispatcher {
    private let session: URLSession
    private let configuration: NetworkConfiguration


    init(configuration: NetworkConfiguration = MusicBrainzAPI()) {
        self.session = URLSession(configuration: URLSessionConfiguration.default)
        self.configuration = configuration
    }

    func execute(request: Request, completion: @escaping ((DispatcherResponse) -> Void)) {
        let urlRequest = prepare(request)
        let task = session.dataTask(with: urlRequest) { (data, _, error) in
            if let data = data {
                completion(.success(data))
            } else {
                completion(.error(error))
            }
        }
        task.resume()
    }
}

private extension NetworkDispatcher {

    func prepare(_ request: Request) -> URLRequest {
        var fullURL = configuration.baseURL.appendingPathComponent(request.path)

        if let queryParams = request.queryParams {
            var urlComponents = URLComponents(url: fullURL, resolvingAgainstBaseURL: false)

            urlComponents?.queryItems = queryParams.map { URLQueryItem(name: "\($0)", value: "\($1)") }

            if let newUrl = urlComponents?.url {
                fullURL = newUrl
            }
        }

        var urlRequest = URLRequest(url: fullURL)
        urlRequest.httpMethod = request.method.rawValue

        return urlRequest
    }
}
