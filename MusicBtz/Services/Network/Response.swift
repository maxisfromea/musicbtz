//
//  Response.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 30/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol Response: Codable {
    associatedtype ResponseDataType

    var created: String { get }
    var count: Int { get }
    var offset: Int { get }
    var data: [ResponseDataType] { get }
}

struct PlacesResponse: Response {
    enum CodingKeys: String, CodingKey {
        case created
        case count
        case offset
        case data = "places"
    }

    typealias ResponseDataType = Place

    let created: String
    let count: Int
    let offset: Int
    let data: [Place]
}

enum PlaceType: String, Codable {
    case other = "Other"
    case studio = "Studio"
    case venue = "Venue"
    case stadium = "Stadium"
    case indoorArena = "Indoor arena"
    case religiousBuilding = "Religious building"
    case educationalInstitution = "Educational institution"
    case pressingPlant = "Pressing plant"
}

struct PlaceCoordinates: Codable {
    let latitude: String
    let longitude: String
}

struct PlaceArea: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case sortName = "sort-name"
    }

    let id: String
    let name: String
    let sortName: String
}

struct PlaceLifeSpan: Codable {
    let begin: String?
    let end: String?
    let ended: Bool?
}

struct Place: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case score
        case name
        case address
        case coordinates
        case area
        case lifeSpan = "life-span"
    }

    let id: String
    let type: PlaceType?
    let score: Int?
    let name: String?
    let address: String?
    let coordinates: PlaceCoordinates?
    let area: PlaceArea?
    let lifeSpan: PlaceLifeSpan?
}
