//
//  Configuration.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol NetworkConfiguration {
    var baseURL: URL { get }
}

struct MusicBrainzAPI: NetworkConfiguration {
    let baseURL = URL(string: "http://musicbrainz.org/ws/2")!
}
