//
//  Coordinator.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: class {
    var rootViewController: UIViewController { get }
    var childCoordinators: [Coordinator] { get set }

    func start()
    func push(_ coordinator: Coordinator)
    func pop(_ coordinator: Coordinator)
}

extension Coordinator {
    func push(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func pop(_ coordinator: Coordinator) {
        var coordinatorIndex = -1
        for (index, value) in childCoordinators.enumerated().reversed() where value === coordinator {
            coordinatorIndex = index
            break
        }

        if coordinatorIndex > -1 {
            childCoordinators.remove(at: coordinatorIndex)
        }
    }
}
