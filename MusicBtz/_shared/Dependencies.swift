//
//  Dependencies.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol HasNetworkService {
    var networkService: Network { get }
}

final class AppDependencies: HasNetworkService {
    let networkService: Network

    init() {
        networkService = NetworkService(dispatcher: NetworkDispatcher())
    }
}
