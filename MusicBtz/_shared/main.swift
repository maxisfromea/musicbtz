//
//  main.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 01/07/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

private func delegateClassName() -> String? {
    return NSClassFromString("XCTestCase") == nil ? NSStringFromClass(AppDelegate.self) : nil
}

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, delegateClassName())
