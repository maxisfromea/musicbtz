//
//  HomeViewModelTests.swift
//  MusicBtzTests
//
//  Created by Maksim Moisja on 01/07/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import XCTest
@testable import MusicBtz

class HomeViewModelTests: XCTestCase {

    var error: String?
    var items: [PinItem]?
    var removedItem: PinItem?
    var isLoading: Bool?

    var showExpectation: XCTestExpectation?
    var removeExpectation: XCTestExpectation?
    var isLoadingExpectation: XCTestExpectation?

    var dependencies: AppDependenciesMock!

    override func setUp() {
        super.setUp()

        dependencies = AppDependenciesMock()
        showExpectation = nil
        removeExpectation = nil
    }

    func testSearchNilQueryDoesNotMakeRequest() {
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: nil)
        XCTAssertNil(isLoading)
    }

    func testSearchEmptyQueryDoesNotMakeRequest() {
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "")
        XCTAssertNil(isLoading)
    }

    func testSearchMakesRequestWhenDataCountIsLesserThanConstant() {
        let count = 5
        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: count, offset: 0, data: createPlaces(count: count))

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        showExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [showExpectation!], timeout: 5)

        XCTAssertEqual(items?.count, count)
        XCTAssertEqual(items?.first?.id, "0")
    }

    func testSearchMakesRequestsWhenDataCountIsBiggerThanConstants() {
        let count = 21
        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: count, offset: 0, data: createPlaces(count: count))

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        showExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [showExpectation!], timeout: 5)

        XCTAssertEqual(items?.count, count)
        XCTAssertEqual(items?.first?.id, "0")
        XCTAssertEqual(items?.last?.id, "20")
    }

    func testDataFiltersLesserThanDefienedLifeSpan() {
        let count = 3
        let incorrectPlace = place(lifeSpan: PlaceLifeSpan(begin: "1900", end: nil, ended: nil))
        let places = createPlaces(count: count) + [incorrectPlace]

        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: count, offset: 0, data: places)

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        showExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [showExpectation!], timeout: 5)

        XCTAssertEqual(items?.count, count)
        XCTAssertEqual(items?.first?.id, "0")
        XCTAssertEqual(items?.last?.id, "2")
    }

    func testDataFiltersNilBeginLifeSpan() {
        let incorrectPlace = place(lifeSpan: nil)

        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: 1, offset: 0, data: [incorrectPlace])

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        showExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [showExpectation!], timeout: 5)

        XCTAssertEqual(items?.count, 0)
    }

    func testRemoveItemIsCalledInTime() {
        let correctPlace = place(lifeSpan: PlaceLifeSpan(begin: "\(Constants.openSince+1)", end: nil, ended: nil))
        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: 1, offset: 0, data: [correctPlace])

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        removeExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [removeExpectation!], timeout: 1.5)

        XCTAssertEqual(removedItem?.id, correctPlace.id)
    }

    func testViewDelegateGetsCalledForError() {
        (dependencies.networkService as? NetworkServiceMock)?.error = "Error"
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")
        showExpectation = XCTestExpectation(description: "Wait for error")
        wait(for: [showExpectation!], timeout: 1)

        XCTAssertEqual(error, "Error")
    }

    func testViewDelegateCallsIsLoadingTrue() {
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")
        XCTAssertEqual(isLoading, true)
    }

    func testViewDelegateCallsIsLoadingFalseOnError() {
        (dependencies.networkService as? NetworkServiceMock)?.error = "Error"
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        isLoadingExpectation = XCTestExpectation(description: "Wait for error")
        wait(for: [isLoadingExpectation!], timeout: 5)

        XCTAssertEqual(isLoading, false)
    }

    func testViewDelegateCallsIsLoadingFalseOnSuccess() {
        let incorrectPlace = place(lifeSpan: nil)

        (dependencies.networkService as? NetworkServiceMock)?.responseData = PlacesResponse(
            created: "2019", count: 1, offset: 0, data: [incorrectPlace])

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = self

        viewModel.search(for: "Test")

        isLoadingExpectation = XCTestExpectation(description: "Wait for data to download")
        wait(for: [isLoadingExpectation!], timeout: 2)

        XCTAssertEqual(isLoading, false)
    }
}

private extension HomeViewModelTests {

    func createPlaces(count: Int) -> [Place] {
        var places = [Place]()
        for i in 0..<count {
            places.append(Place(
                id: "\(i)",
                type: .indoorArena,
                score: i,
                name: "Place\(i)",
                address: nil,
                coordinates: PlaceCoordinates(latitude: "2019", longitude: "2019"),
                area: nil,
                lifeSpan: PlaceLifeSpan(begin: "\(Constants.openSince+1)", end: nil, ended: nil)))
        }
        return places
    }

    func place(lifeSpan: PlaceLifeSpan?) -> Place {
        return Place(
            id: "10",
            type: nil,
            score: nil,
            name: nil,
            address: nil,
            coordinates: PlaceCoordinates(latitude: "10", longitude: "10"),
            area: nil,
            lifeSpan: lifeSpan)
    }
}

extension HomeViewModelTests: HomeVMVD {
    func show(items: [PinItem]) {
        self.items = items
        showExpectation?.fulfill()
    }

    func remove(item: PinItem) {
        removedItem = item
        removeExpectation?.fulfill()
    }

    func updateLoadingStatus(isLoading: Bool) {
        self.isLoading = isLoading
        isLoadingExpectation?.fulfill()
    }

    func show(error: String) {
        self.error = error
        showExpectation?.fulfill()
    }
}
