//
//  Mocks.swift
//  MusicBtzTests
//
//  Created by Maksim Moisja on 01/07/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
@testable import MusicBtz

final class NetworkServiceMock: Network {
    var responseData: PlacesResponse?
    var error: String?

    func getPlaces(by query: String, offset: Int, completion: @escaping (ResponseData<PlacesResponse>) -> ()) {
        if let data = responseData {
            let dataStartIndex = Constants.pageSize * offset
            let dataEndIndex = min(data.count, dataStartIndex+Constants.pageSize)

            let correctPlaces = Array(data.data[dataStartIndex..<dataEndIndex])
            let modifiedRespone = PlacesResponse(
                created: data.created,
                count: data.count,
                offset: data.offset,
                data: correctPlaces)

            completion(.success(modifiedRespone))
        } else if let err = error {
            completion(.error(err))
        }
    }
}

final class AppDependenciesMock: HasNetworkService {
    var networkService: Network = NetworkServiceMock()
}
