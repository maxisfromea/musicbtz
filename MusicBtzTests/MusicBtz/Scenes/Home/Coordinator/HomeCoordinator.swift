//
//  HomeCoordinator.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 30/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import UIKit

final class HomeCoordinator: Coordinator {
    typealias Dependencies = HasNetworkService
    private let dependencies: Dependencies

    var rootViewController: UIViewController
    var childCoordinators = [Coordinator]()

    init(rootController: UINavigationController, dependencies: Dependencies) {
        self.rootViewController = rootController
        self.dependencies = dependencies
    }

    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else {

                return
        }
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewController.viewModel = viewModel

        (rootViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
    }


}
