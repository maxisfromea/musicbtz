//
//  HomeViewController.swift
//  MusicBtz
//
//  Created by Maksim Moisja on 30/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: UIViewController {

    @IBOutlet private weak var map: MKMapView!

    private var pinItems = [PinItem]()

    var viewModel: HomeViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.view = self
        navigationController?.navigationBar.isHidden = true
    }
}

// MARK: - HomeViewModelViewDelegate
extension HomeViewController: HomeVMVD {

    func show(items: [PinItem]) {
        DispatchQueue.main.async { [map] in
            map?.addAnnotations(items)
        }
    }

    func remove(item: PinItem) {
        DispatchQueue.main.async { [map] in
            map?.removeAnnotation(item)
        }
    }

    func updateLoadingStatus(isLoading: Bool) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = isLoading
        }
    }

    func show(error: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true)
        }
    }
}

// MARK: - UISearchBarDelegate
extension HomeViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.search(for: searchBar.text)
        view.endEditing(false)
    }
}
