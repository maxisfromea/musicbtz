//
//  NetworkService.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol Network {
    func getPlaces(by query: String, offset: Int,
                   completion: @escaping (ResponseData<PlacesResponse>) -> ())
}

final class NetworkService: Network {

    private let dispatcher: Dispatcher

    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }

    func getPlaces(by query: String, offset: Int,
                   completion: @escaping (ResponseData<PlacesResponse>) -> ()) {

        let request = PlacesRequest(query: query, pageNumber: offset)
        dispatcher.execute(request: request) { (response) in
            ResponseParser.parse(response: response, completion: completion)
        }
    }

}





