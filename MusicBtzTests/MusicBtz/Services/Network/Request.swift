//
//  Request.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
}

public protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var queryParams: [String: Any]? { get }
}


struct PlacesRequest: Request {
    let path: String = "/place"
    let method: HTTPMethod = .get
    var queryParams: [String: Any]? = [String : Any]()

    init(query: String, pageNumber: Int) {
        queryParams?["limit"] = Constants.pageSize
        queryParams?["offset"] = pageNumber
        queryParams?["query"] = query
        queryParams?["fmt"] = "json"
    }
}
