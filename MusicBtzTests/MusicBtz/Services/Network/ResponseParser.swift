//
//  ResponseParser.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

public enum ResponseData<T: Codable> {
    case error(String)
    case success(T)
}

struct ResponseParser {

    static func parse<T>(response: DispatcherResponse,
                         completion: @escaping (ResponseData<T>) -> Void) {

        switch response {
        case .error(let error):
            if let error = error {
                completion(.error(error.localizedDescription))
            } else {
                completion(.error("Something went wrong"))
            }

        case .success(let data):
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                completion(.success(decodedData))
            } catch {
                debugPrint(error)
                completion(.error(error.localizedDescription))
            }
        }
    }
}
