//
//  AppCoordinator.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator: Coordinator {
    private let dependencies: AppDependencies
    private let window: UIWindow

    let rootViewController: UIViewController

    var childCoordinators = [Coordinator]()

    init(_ window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
        dependencies = AppDependencies()
    }

    func start() {
        let coordinator = HomeCoordinator(
            rootController: rootViewController as! UINavigationController,
            dependencies: dependencies)
        push(coordinator)
        coordinator.start()

        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}
