//
//  Constants.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

struct Constants {
    static let pageSize = 20
    static let openSince: TimeInterval = 1990
}
